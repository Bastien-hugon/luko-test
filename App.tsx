import React from 'react';

import RootComponent from './src'

export default function App() {
  return (
    <RootComponent />
  );
}