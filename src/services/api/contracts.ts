const get = () => {
    return [
        {
            id: 1,
            name: 'Contract #1',
        },
        {
            id: 2,
            name: 'Contract #2',
        },
        {
            id: 3,
            name: 'Contract #3',
        },
        {
            id: 4,
            name: 'Contract #4',
        },
    ]
};

export default {
    get,
};
