// Core
import React, { useEffect, useState } from 'react';
import {
    NavigationParams,
    NavigationScreenProp,
    NavigationState,
} from 'react-navigation';
import { useDispatch } from 'react-redux';

// Components
import {
    SafeAreaView,
    KeyboardAvoidingView,
    ScrollView,
    Platform,
    StyleSheet,
    View,
    Text,
    Alert,
} from 'react-native';

import EditLayout from '../../../components/layouts/EditLayout';

import ImportPhoto from '../../../components/ui/ImportPhoto';

import TextInput from '../../../components/forms/TextInput';
import Select from '../../../components/forms/Select';

import { InventoryItem } from '../types';
import ContractsService from '../../../services/api/contracts';
import { canPushItemToInventory } from '../../../helpers/inventory';

import InventoryActions from '../../../store/actions/inventory/';

import Colors from '../../../components/colors.json';

// Interfaces
interface AddItemProps {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
};

// Globals
const Categories: string[] = [
    'Art',
    'Electronics',
    'Jewelry',
    'Music Instruments',
];

function AddItem({ navigation }: AddItemProps) {
    // Photos
    const [mainPhoto, setMainPhoto] = useState<any>(null);
    const [otherPhoto, setOtherPhotos] = useState<any>(null);
    const [receipt, setReceipt] = useState<any>(null);

    // Inputs
    const [name, setName] = useState<string>('');
    const [purchaseDate, setPurchaseDate] = useState<string>('');
    const [value, setValue] = useState<string>('');
    const [description, setDescription] = useState<string | null>(null);
    const [selectedContract, setSelectedContract] = useState<React.ReactText | null>(null);
    const [selectedCategory, setSelectedCategory] = useState<React.ReactText | null>(null);

    // Simulated fetched data
    const [contractsData, setContractsData] = useState<any>([]);

    const dispatch = useDispatch();

    const saveObject = () => {
        const item: InventoryItem = {
            id: Math.floor(Math.random() * 10000),
            name,
            contract: String(selectedContract),
            category: String(selectedCategory),
            purchaseDate,
            purchaseValue: parseFloat(value.split(' €').join('').split(' ').join('')),
            description,
            photos: [mainPhoto, otherPhoto],
            receipts: receipt,
        };
        const canPushItem: Boolean = canPushItemToInventory(item);

        if (!canPushItem) {
            Alert.alert('Luko Test', 'You cannot have a contract exceeding 40 000€');
            return;
        }

        dispatch(InventoryActions.addItem(item));
        navigation.goBack();
    };

    useEffect(() => {
        setContractsData(ContractsService.get());
    }, []);

    return (
        <EditLayout
            title="New Object"
            onSave={saveObject}
            onClose={() => navigation.goBack()}
            savable={Boolean(mainPhoto && receipt && name && purchaseDate && value && selectedCategory && selectedContract)}
        >
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={styles.container}
            >
                <SafeAreaView>
                    <ScrollView
                        contentContainerStyle={styles.contentContainer}
                    >
                        <View style={styles.addMainPhoto}>
                            <ImportPhoto
                                title="Add Photo"
                                iconName="camera"
                                pack="Entypo"
                                onSelect={setMainPhoto}
                            />
                        </View>
                        <TextInput
                            placeholder="Name"
                            onChangeText={setName}
                            />
                        <Select
                            items={contractsData.map((contract: any) => contract.name)}
                            placeholder="Contract"
                            onSelect={setSelectedContract}
                        />
                        <Select
                            items={Categories}
                            placeholder="Category"
                            onSelect={setSelectedCategory}
                        />
                        <TextInput
                            placeholder="Purchase Date"
                            onChangeText={setPurchaseDate}
                            maskType="datetime"
                            mask={{
                                format: 'DD/MM/YYYY'
                            }}
                        />
                        <TextInput
                            placeholder="Purchase Value"
                            onChangeText={setValue}
                            maskType="money"
                            mask={{
                                precision: 2,
                                separator: '.',
                                delimiter: ' ',
                                unit: '',
                                suffixUnit: '€'
                            }}
                        />
                        <TextInput
                            placeholder="Description (optional)"
                            onChangeText={setDescription}
                        />
                        <Text style={styles.addOtherPhotoText}>
                            Documents
                        </Text>
                        <View style={styles.addOtherPhotoView}>
                            <ImportPhoto
                                title="Add Receipt"
                                iconName="file-contract"
                                pack="FontAwesome5"
                                onSelect={setReceipt}
                            />
                            <ImportPhoto
                                title="Add Photos"
                                iconName="camera"
                                pack="Entypo"
                                onSelect={setOtherPhotos}
                                style={styles.marginLeft}
                            />
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </KeyboardAvoidingView>
        </EditLayout>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    contentContainer: {
        paddingHorizontal: 20,
    },

    addMainPhoto: {
        alignItems: 'center',
        marginVertical: 40,
    },

    addOtherPhotoText: {
        color: Colors.blueyGrey,
        fontFamily: 'Avenir-Medium',
        fontSize: 12,
        marginTop: 20,
        marginBottom: 10,
    },

    addOtherPhotoView: {
        flexDirection: 'row',
        marginBottom: 50,
    },

    marginLeft: {
        marginLeft: 20,
    }
});

export default AddItem;
