// Core
import React, { useCallback, useState } from 'react';

import {
    NavigationParams,
    NavigationScreenProp,
    NavigationState,
} from 'react-navigation';
import { useSelector } from 'react-redux';

// Components
import {
    View,
    FlatList,
    StyleSheet,
} from 'react-native';

import MainLayout from '../../components/layouts/MainLayout';
import ActionButton from '../../components/forms/ActionButton';
import SearchInput from '../../components/forms/SearchInput';
import InventoryCard from '../../components/ui/InventoryCard';


import Routes from '../../navigation/routes';

import Colors from '../../components/colors.json';

// Interfaces
interface InventoryProps {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
};

interface RenderItemProps {
    item: any,
};

function Inventory({ navigation }: InventoryProps) {
    const [search, setSearch] = useState<string>('');
    const inventoryItems = useSelector((rdc) => rdc.inventory.items);

    const goToAddItemScreen = () => {
        navigation.navigate(Routes.INVENTORY_ADD);
    };

    const renderItem = useCallback(({ item }: RenderItemProps) => (
        <InventoryCard item={item} />
    ), []);

    return (
        <MainLayout
            title="Inventory"
            accessory={<ActionButton pack="Entypo" name="plus" onPress={goToAddItemScreen} />}
        >
            <View style={styles.searchContainer}>
                <SearchInput
                    onChangeText={setSearch}
                    placeholder={`Search ${inventoryItems.length} items`}
                />
            </View>
            <FlatList
                data={inventoryItems.filter((item: any) => item.name.includes(search))}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
                numColumns={2}
                style={styles.main}
                contentContainerStyle={styles.contentContainer}
            />
        </MainLayout>
    )
};

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: Colors.paleGreyTwo,
    },

    contentContainer: {
        paddingBottom: 20,
    },

    searchContainer: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: Colors.white,
    }
})

export default Inventory;
