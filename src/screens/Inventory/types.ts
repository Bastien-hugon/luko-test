import { ImageInfo } from "expo-image-picker/build/ImagePicker.types";

export interface InventoryItem {
    id: number,
    name: string,
    contract: string,
    category: string,
    purchaseDate: string,
    purchaseValue: number,
    description: string | null,
    photos: ImageInfo[],
    receipts: ImageInfo,
};
