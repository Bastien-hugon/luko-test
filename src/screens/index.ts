// Other routes
// ...

// Inventory
import Inventory from './Inventory';
import InventoryAdd from './Inventory/AddItem';

export default {
    Inventory,
    InventoryAdd,
};