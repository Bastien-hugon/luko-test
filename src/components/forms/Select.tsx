// Core
import React, { useState } from 'react';

// Components
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';

import { FontAwesome } from '@expo/vector-icons';

import { Picker } from '@react-native-picker/picker';

import Colors from '../colors.json';

// Interfaces
interface SelectProps {
    onSelect?: (value: React.ReactText) => void,
    placeholder?: string,
    items: string[],
};

function Select({ onSelect = () => {}, placeholder = '', items }: SelectProps) {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const [value, setValue] = useState<React.ReactText | undefined>(undefined);

    const canDisplayPlaceholder = value === undefined && !isOpen && placeholder.length;
    const canDisplayLabel = (value !== undefined || isOpen) && placeholder.length;
    
    const displayLabel = () => {
        if (canDisplayLabel) {
            return (
                <Text style={styles.label}>
                    {placeholder}
                </Text>
            );
        }
        return null;
    }

    const handleOnChange = (itemValue: React.ReactText, itemIndex: number) => {
        setValue(itemValue);
        onSelect(itemValue);
        setIsOpen(false)
    }

    return (
        <View>
            {displayLabel()}
            <TouchableOpacity
                onPress={() => setIsOpen(true)}
                style={styles.input}
            >
                {canDisplayPlaceholder && (
                    <View style={styles.placeholderContainer}>
                        <Text style={styles.placeholder}>{placeholder}</Text>
                        <FontAwesome name="angle-down" color={Colors.blueyGrey} size={16} />
                    </View>
                )}
                {isOpen && (
                    <Picker
                        selectedValue={value}
                        onValueChange={handleOnChange}
                    >
                        {items.map((item, key) => (
                            <Picker.Item key={`${key}-key`} label={item} value={item} />
                        ))}
                    </Picker>
                )}
                {!isOpen && !canDisplayPlaceholder && (
                    <View style={styles.placeholderContainer}>
                        <Text style={styles.value}>{value}</Text>
                        <FontAwesome name="angle-down" color={Colors.black} size={16} />
                    </View>
                )}
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    label: {
        position: 'absolute',
        top: 10,
        color: Colors.blueyGrey,
        fontFamily: 'Avenir-Medium',
        fontSize: 12,
    },

    input: {
        color: Colors.black,
        paddingTop: 28,
        paddingBottom: 10,
        borderBottomColor: Colors.paleGreyThree,
        borderBottomWidth: 1,
    },

    placeholderContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    placeholder: {
        fontFamily: 'Avenir-Medium',
        fontSize: 16,
        color: Colors.blueyGrey,
    },

    value: {
        fontFamily: 'Avenir-Medium',
        fontSize: 16,
        color: Colors.black,
    }

});

export default Select;
