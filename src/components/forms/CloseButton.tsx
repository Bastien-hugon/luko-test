// Core
import React from 'react';

// Components
import {
    TouchableOpacity,
} from 'react-native';

import { AntDesign } from '@expo/vector-icons';

import Colors from '../colors.json';

// Interfaces
interface CloseButtonProps {
    onPress: () => void,
    style?: React.CSSProperties,
};

function CloseButton({ onPress = () => {}, style = {} }: CloseButtonProps) {

    return (
        <TouchableOpacity
            onPress={onPress}
            {...style}
        >
            <AntDesign name="close" size={24} color={Colors['black']} />
        </TouchableOpacity>
    );
};

export default CloseButton;
