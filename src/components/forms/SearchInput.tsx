// Core
import React from 'react';

// Components
import {
    View,
    TextInput,
    StyleSheet,
} from 'react-native';
import { AntDesign } from '@expo/vector-icons'; 

import Colors from '../colors.json';

// Interfaces
interface SearchInputProps {
    onChangeText: (value: string) => void,
    placeholder?: string,
};

function SearchInput({ onChangeText, placeholder = '' }: SearchInputProps) {
    return (
        <View style={styles.container}>
            <AntDesign style={styles.icon} name="search1" size={18} color={Colors.blueyGrey} />
            <TextInput
                selectionColor={Colors.black}
                placeholderTextColor={Colors.blueyGrey}
                onChangeText={onChangeText}
                placeholder={placeholder}
                style={styles.input}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.paleGrey,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },

    icon: {
        marginLeft: 10,
    },

    input: {
        color: Colors.black,
        fontFamily: 'Avenir-Medium',
        fontSize: 16,
        paddingVertical: 7,
        marginLeft: 10,
        width: '88%',
    },
});

export default SearchInput;
