// Core
import React, { useState } from 'react';

// Components
import {
    TextInput as NativeInput,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import {
    TextInputMask,
    TextInputMaskOptionProp,
} from 'react-native-masked-text';

import Colors from '../colors.json';

// Interfaces
interface CustomTextInputProps {
    onChangeText: (value: string) => void,
    placeholder?: string,
    mask?: TextInputMaskOptionProp,
    maskType?: any,
};

function TextInput({
    onChangeText,
    placeholder = '',
    mask,
    maskType,
}: CustomTextInputProps) {
    const [isFocused, setIsFocused] = useState<boolean>(false);
    const [value, setValue] = useState<string>('');

    const overrideOnChangeText = (text: string) => {
        setValue(text);
        onChangeText(text);
    }

    const displayLabel = () => {
        const canDisplayLabel = value.length && placeholder.length;

        if (canDisplayLabel) {
            return (
                <Text style={styles.label}>
                    {placeholder}
                </Text>
            );
        }
        return null;
    }

    return (
        <View>
            {displayLabel()}
            {!mask && (
                <NativeInput
                    style={[styles.input, isFocused && styles.focused]}
                    onFocus={() => setIsFocused(true)}
                    onBlur={() => setIsFocused(false)}
                    selectionColor={Colors.black}
                    placeholderTextColor={Colors.blueyGrey}
                    onChangeText={overrideOnChangeText}
                    placeholder={placeholder}
                />
            )}

            {mask && (
                <TextInputMask
                    style={[styles.input, isFocused && styles.focused]}
                    onFocus={() => setIsFocused(true)}
                    onBlur={() => setIsFocused(false)}
                    selectionColor={Colors.black}
                    placeholderTextColor={Colors.blueyGrey}
                    type={maskType}
                    options={mask}
                    onChangeText={overrideOnChangeText}
                    value={value}
                    placeholder={placeholder}
                />
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    input: {
        color: Colors.black,
        fontFamily: 'Avenir-Medium',
        fontSize: 16,
        paddingTop: 28,
        paddingBottom: 10,
        borderBottomColor: Colors.paleGreyThree,
        borderBottomWidth: 1,
    },

    focused: {
        borderBottomColor: Colors.blue,
        borderBottomWidth: 2,
    },

    label: {
        position: 'absolute',
        top: 10,
        color: Colors.blueyGrey,
        fontFamily: 'Avenir-Medium',
        fontSize: 12,
    }
});

export default TextInput;
