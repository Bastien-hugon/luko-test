// Core
import React from 'react';

// Components
import {
    View,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import {
    AntDesign,
    FontAwesome,
    FontAwesome5,
    Entypo,
} from '@expo/vector-icons';

import Colors from '../../components/colors.json';

// Interfaces
interface ActionButtonProps {
    pack: string,
    name: string,
    onPress: () => void,
};

const Icons: any = { AntDesign, FontAwesome, FontAwesome5, Entypo };

function ActionButton({ pack = 'FontAwesome', name, onPress = () => {} }: ActionButtonProps) {

    const Icon: any = Icons[pack];

    return (
        <TouchableOpacity
            onPress={onPress}
            style={styles.button}
        >
            <Icon name={name} size={24} color={Colors.white} />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: Colors.blue,
        width: 28,
        height: 28,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,

    },
});

export default ActionButton;
