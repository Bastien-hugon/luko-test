// Core
import React, { useState, useEffect } from 'react';

// Components
import {
    Text,
    TouchableOpacity,
    StyleSheet,
    Image,
} from 'react-native';

import {
    AntDesign,
    FontAwesome,
    FontAwesome5,
    Entypo,
} from '@expo/vector-icons';

import * as ImagePicker from 'expo-image-picker';
import { ImageInfo } from 'expo-image-picker/build/ImagePicker.types';

import Colors from '../colors.json';

// Interfaces
interface ImportPhotoProps {
    title: string,
    pack?: string,
    iconName: string,
    onSelect?: (image: ImageInfo) => void,
    style?: any,
};

const Icons: any = { AntDesign, FontAwesome, FontAwesome5, Entypo };


function ImportPhoto({
    title,
    pack = 'FontAwesome',
    iconName,
    onSelect = () => {},
    style = {},
}: ImportPhotoProps) {
    const [image, setImage] = useState<ImageInfo | null>(null);

    const Icon: any = Icons[pack];

    const pickImage = async () => {
        const { status } = await ImagePicker.requestCameraPermissionsAsync();
        if (status !== 'granted') {
            alert('Sorry, we need camera permissions to make this work!');
            return;
        }

        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
        
        if (!result.cancelled) {
            setImage(result);
            onSelect(result);
        }
    };

    return (
        <TouchableOpacity
            onPress={pickImage}
            style={[styles.container, style]}
        >
            {image && (
                <Image
                    source={{ uri: image?.uri }}
                    style={styles.image}
                />
            )}
            {!image && (
                <>
                    <Icon name={iconName} color={Colors.blue} size={32} />
                    <Text style={styles.title}>{title}</Text>
                </>
            )}
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: 128,
        height: 128,
        borderRadius: 14,
        borderStyle: "dashed",
        borderWidth: 2,
        borderColor: 'rgba(147, 153, 172, 0.4)',
        alignItems: 'center',
        justifyContent: 'center',
    },

    title: {
        fontFamily: 'Avenir-Medium',
        fontSize: 16,
    },

    image: {
        width: 128,
        height: 128,
        resizeMode: 'cover',
        borderRadius: 14,
    }
});

export default ImportPhoto;
