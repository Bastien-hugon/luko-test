// Core
import React from 'react';

// Components
import {
    View,
    StyleSheet,
    Text,
    Image,
    Dimensions,
} from 'react-native';

import { InventoryItem } from '../../screens/Inventory/types';

import Colors from '../colors.json';

// Interfaces
interface InventoryCardProps {
    item: InventoryItem,
};

function InventoryCard({ item }: InventoryCardProps) {
    return (
        <View style={styles.main}>
            <View style={styles.card}>
                <Image
                    source={{ uri: item.photos[0].uri }}
                    style={styles.thumbnail}
                />
                <View style={styles.body}>
                    <Text style={styles.title}>{item.name}</Text>
                    <Text style={styles.price}>{String(item.purchaseValue).replace(/(.)(?=(\d{3})+$)/g,'$1 ')}€</Text>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    main: {
        width: Dimensions.get('window').width / 2,
        paddingHorizontal: 10,
        paddingTop: 20,
    },

    card: {
        borderRadius: 14,
        backgroundColor: Colors.white,
        shadowColor: Colors.black12,
        shadowOffset: {
          width: 0,
          height: 10
        },
        shadowRadius: 20,
        shadowOpacity: 1,
        elevation: 10,
    },

    thumbnail: {
        borderTopLeftRadius: 14,
        borderTopRightRadius: 14,
        width: '100%',
        height: 158,
        resizeMode: 'cover',
    },

    body: {
        paddingHorizontal: 20,
        paddingVertical: 16,
    },

    title: {
        fontFamily: 'Avenir-Medium',
        color: Colors.black,
        fontSize: 16,
        textTransform: 'capitalize',
        minHeight: 35,
    },

    price: {
        fontFamily: 'Avenir-Medium',
        color: Colors.blueyGrey,
        fontSize: 14,
        marginTop: 10,
    }
});

export default InventoryCard;
