// Core
import React from 'react';

// Components
import {
    NativeModules,
    Platform,
    View,
    StyleSheet,
    Text,
} from 'react-native';

import Colors from '../colors.json';

// Interfaces
interface MainLayoutProps {
    title: string,
    accessory: React.ReactNode,
    children: React.ReactNode,
};

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 44 : NativeModules.StatusBarManager.HEIGHT;

function MainLayout({ title, accessory = null, children = null }: MainLayoutProps) {
    return (
        <>
            <View style={styles.main}>
                <View style={styles.titleView}>
                    <Text style={styles.titleText}>{title}</Text>
                    {accessory}
                </View>
            </View>
            {children}
        </>
    );
};

const styles = StyleSheet.create({
    main: {
        backgroundColor: Colors['white'],
    },

    titleView: {
        marginTop: STATUSBAR_HEIGHT + 48,
        flexDirection: 'row',
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
    },

    titleText: {
        color: Colors['black'],
        fontSize: 34,
        fontFamily: 'Avenir-Bold',
        letterSpacing: 0,
    }
});

export default MainLayout;
