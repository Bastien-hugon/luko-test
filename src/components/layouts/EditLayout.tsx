// Core
import React from 'react';

// Components
import {
    NativeModules,
    Platform,
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
} from 'react-native';

import CloseButton from '../forms/CloseButton';

import Colors from '../colors.json';

// Interfaces
interface EditLayoutProps {
    title: string,
    onClose?: () => void,
    onSave?: () => void,
    savable?: boolean,
    children?: React.ReactNode,
};

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 44 : NativeModules.StatusBarManager.HEIGHT;

function EditLayout({
    title,
    onClose = () => {},
    onSave = () => {},
    savable = true,
    children = null,
}: EditLayoutProps) {
    return (
        <View style={styles.main}>
            <View style={styles.container}>
                <View style={styles.headerView}>
                    <CloseButton
                        onPress={onClose}
                    />

                    <Text style={styles.titleText}>{title}</Text>

                    <TouchableOpacity
                        onPress={onSave}
                        disabled={!savable}
                    >
                        <Text
                            style={[ styles.saveText, savable && styles.activeText ]}
                        >
                            Save
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
            {children}
        </View>
    );
};

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: Colors.white,
    },

    container: {
        borderBottomColor: 'rgba(147, 153, 172, 0.24)',
        borderBottomWidth: 1,
    },

    headerView: {
        marginTop: STATUSBAR_HEIGHT,
        height: 44,
        flexDirection: 'row',
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
    },

    titleText: {
        color: Colors.black,
        fontSize: 16,
        fontFamily: 'Avenir-Bold',
        marginLeft: 15,
    },

    saveText: {
        color: Colors.blueyGrey,
        fontSize: 16,
        fontFamily: 'Avenir-Bold',
    },

    activeText: {
        color: Colors.blue,
    }
});

export default EditLayout;
