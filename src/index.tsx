import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import * as Font from 'expo-font';
import AppLoading from 'expo-app-loading';

import AppNavigator from './navigation/app.navigator';

import { store, persistor } from './store';

const fetchFonts = () => {
    return Font.loadAsync({
        'Avenir-Medium': require('../assets/fonts/Metropolis-Medium.otf'),
        'Avenir-Bold': require('../assets/fonts/Metropolis-Bold.otf'),
    });
};

export default function() {
    const [dataLoaded, setDataLoaded] = useState(false);

    if (!dataLoaded) {
        return (
            <AppLoading
                startAsync={fetchFonts}
                onFinish={() => setDataLoaded(true)}
                onError={console.warn}
            />
        );
    }

    return (
        <Provider store={store}>
            <PersistGate
                loading={null}
                persistor={persistor}
            >
                <AppNavigator />
            </PersistGate>
        </Provider>
    )
}