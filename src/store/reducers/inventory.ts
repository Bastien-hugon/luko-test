import update from 'immutability-helper';
import { AnyAction } from 'redux';

import ActionsTypes, { InventoryState } from '../actions/inventory/types';

const initialState: InventoryState = {
    items: new Array(),
};

const addItem = (state: InventoryState, action: AnyAction) => {
    return update(state, {
        items: {
            $unshift: [ action.payload ],
        },
    });
};

const inventory = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case ActionsTypes.INVENTORY_ADD_ITEM:
            return addItem(state, action);
        default:
            return state;
    }
};

export default inventory;
