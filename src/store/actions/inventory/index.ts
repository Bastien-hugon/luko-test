import ActionTypes from './types';
import { InventoryItem } from '../../../screens/Inventory/types';

const addItem = function(item: InventoryItem) {
    return {
        type: ActionTypes.INVENTORY_ADD_ITEM,
        payload: item,
    }
};

export default {
    addItem,
};
