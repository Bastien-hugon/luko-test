import { InventoryItem } from '../../../screens/Inventory/types';

export default {
    INVENTORY_ADD_ITEM: 'INVENTORY_ADD_ITEM'
};

export interface InventoryState {
    items: InventoryItem[],
};

interface InventoryActionAdd {
    type: string,
    payload: InventoryItem,
}

export type InventoryAction = InventoryActionAdd;