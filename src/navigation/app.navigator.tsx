import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Routes from './routes';
import Screens from '../screens';

const Stack = createStackNavigator();

function AppNavigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator
                headerMode="none"
                initialRouteName={Routes.INVENTORY}
            >
                <Stack.Screen name={Routes.INVENTORY} component={Screens.Inventory} />
                <Stack.Screen name={Routes.INVENTORY_ADD} component={Screens.InventoryAdd} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default AppNavigator;