import { store } from '../store';
import { InventoryItem } from '../screens/Inventory/types';

/**
 * Verify the contracts in the redux store
 *  to add an item in the Inventory.
 * @param newItem Item to verify in the store
 * @returns Boolean
 */
export function canPushItemToInventory(newItem: InventoryItem): Boolean {
    const state = store.getState().inventory;
    const maxContractValue = 40000;

    const contractValue = state.items
        .filter((item) => item.contract === newItem.contract)
        .reduce((acc, item) => acc + item.purchaseValue, 0);
    
    return contractValue + newItem.purchaseValue <= maxContractValue;
};
